package by.itacademy.lydiamoiseenko.unittesting;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
// Sum
// Equivalence Partitions
    @Test
    public void testSum1() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(4, 6);

        Assert.assertEquals(10, actual);
    }
    @Test
    public void testSum2() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(13, 15);

        Assert.assertEquals(20, actual);
    }
    @Test
    public void testSum3() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(16, 17);

        Assert.assertEquals(30, actual);
    }
    @Test
    public void testSum4() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(50, 60);

        Assert.assertEquals(40, actual);
    }
    @Test
    public void testSum5() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(-3, 40);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testSum6() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(5, -8);

        Assert.assertEquals(0, actual);
    }
// Sum
// Boundary values
    @Test
    public void testSum01() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(-1, -1);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testSum02() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(0, 0);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testSum03() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(0, -1);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testSum04() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(9, 9);

        Assert.assertEquals(18, actual);
    }
    @Test
    public void testSum05() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(10, 10);

        Assert.assertEquals(20, actual);
    }
    @Test
    public void testSum06() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(14, 14);

        Assert.assertEquals(20, actual);
    }
    @Test
    public void testSum07() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(15, 15);

        Assert.assertEquals(30, actual);
    }
    @Test
    public void testSum08() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(19, 19);

        Assert.assertEquals(30, actual);
    }
    @Test
    public void testSum09() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(20, 20);

        Assert.assertEquals(40, actual);
    }
    @Test
    public void testSum010() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MAX_VALUE, Integer.MAX_VALUE);

        Assert.assertEquals(40, actual);
    }
    @Test
    public void testSum011() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(Integer.MIN_VALUE, Integer.MIN_VALUE);

        Assert.assertEquals(0, actual);
    }
// Divide
// Equivalence Partitions
    @Test
    public void testDivide1() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(5, -10);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testDivide2() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(20, 4);

        Assert.assertEquals(5, actual);
    }
    @Test
    public void testDivide3() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(-7, 20);

        Assert.assertEquals(1, actual);
    }
// Divide
// Boundary values
    @Test
    public void testDivide01() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(Integer.MIN_VALUE, -1);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testDivide02() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(-3, 0);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testDivide03() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(1, 1);

        Assert.assertEquals(1, actual);
    }
    @Test
    public void testDivide04() {
        Calculator calculator = new Calculator();
        int actual = calculator.divide(Integer.MAX_VALUE, 2);

        Assert.assertEquals(1073741823, actual);
    }

// Multiply
// Equivalence Partitions
    @Test
    public void testMultiply1() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(-7, 6);

        Assert.assertEquals(-1, actual);
    }
    @Test
    public void testMultiply2() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(5, -20);

        Assert.assertEquals(-1, actual);
    }
    @Test
    public void testMultiply3() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(3, 8);

        Assert.assertEquals(24, actual);
    }
    @Test
    public void testMultiply4() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(20, 45);

        Assert.assertEquals(100, actual);
    }
    @Test
    public void testMultiply5() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(50, 70);

        Assert.assertEquals(1000, actual);
    }
    @Test
    public void testMultiply6() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(500, 10);

        Assert.assertEquals(10000, actual);
    }
    @Test
    public void testMultiply7() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(5, 375);

        Assert.assertEquals(10000, actual);
    }
// Multiply
// Boundary values
    @Test
    public void testMultiply01() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(Integer.MIN_VALUE, -1);

        Assert.assertEquals(-1, actual);
    }
    @Test
    public void testMultiply02() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(0, 0);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testMultiply03() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(9, 9);

        Assert.assertEquals(81, actual);
    }
    @Test
    public void testMultiply04() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(10, 10);

        Assert.assertEquals(100, actual);
    }
    @Test
    public void testMultiply05() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(37, 27);

        Assert.assertEquals(100, actual);
    }
    @Test
    public void testMultiply06() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(50, 20);

        Assert.assertEquals(1000, actual);
    }
    @Test
    public void testMultiply07() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(99, 99);

        Assert.assertEquals(1000, actual);
    }
    @Test
    public void testMultiply08() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(100, 100);

        Assert.assertEquals(10000, actual);
    }
    @Test
    public void testMultiply09() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(Integer.MAX_VALUE, Integer.MAX_VALUE);

        Assert.assertEquals(10000, actual);
    }
// Subtract
// Equivalence Partitions
    @Test
    public void testDSubtract1() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(-7, -5);

        Assert.assertEquals(5, actual);
    }
    @Test
    public void testDSubtract2() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(20, -8);

        Assert.assertEquals(20, actual);
    }
    @Test
    public void testDSubtract3() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(100, 15);

        Assert.assertEquals(85, actual);
    }
// Subtract
// Boundary values
    @Test
    public void testDSubtract01() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(0, 0);

        Assert.assertEquals(0, actual);
    }
    @Test
    public void testDSubtract02() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(1, -1);

        Assert.assertEquals(1, actual);
    }
    @Test
    public void testDSubtract03() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(Integer.MAX_VALUE, Integer.MIN_VALUE);

        Assert.assertEquals(2147483647, actual);
    }
    @Test
    public void testDSubtract04() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(-1, 0);

        Assert.assertEquals(0, actual);
    }
}