package by.itacademy.lydiamoiseenko.unittesting;

public class Calculator {
    public int sum(int a, int b) {
        if ((a >= 0 & a < 10) && (b >= 0 & b < 10))
            return a + b;
        else if (a >= 10 & a < 20 & b >= 10 & b < 20 && a + b < 30 & a + b >= 20)
            return 20;
        else if (a >= 10 & a < 20 & b >= 10 & b < 20 && a + b >= 30 & a + b < 40)
            return 30;
        else if (a >= 20 & b >= 20)
            return 40;
        else return 0;
    }
    public int divide(int a, int b) {
        if (b <= 0)
            return 0;
        else if (a >= b)
            return a / b;
        else
            return 1;
    }
    public int multiply(int a, int b) {
        if (a < 0 || b < 0)
            return -1;
        else if ((a >= 0 & a < 10) && (b >= 0 & b < 10))
            return a * b;
        else if (a >= 10 & a < 100 & b >= 10 & b < 100 && a * b >= 100 & a * b < 1000)
            return 100;
        else if (a >= 10 & a < 100 & b >= 10 & b < 100 && a * b >= 1000 & a * b < 10000)
            return 1000;
        else return 10000;
    }
    public int subtract(int a, int b) {
        if (a >= b & a >= 0 & b >= 0)
            return a - b;
        else if (a >= b & a >= 0 & b < 0)
            return  a;
        else return -b;
    }
}